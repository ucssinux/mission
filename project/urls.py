from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
# Uncomment the next two lines to enable the admin:
from django.contrib import admin

admin.autodiscover()
import settings

urlpatterns = patterns('',
                       # Examples:
                       # url(r'^$', 'project.views.home', name='home'),
                       # url(r'^project/', include('project.foo.urls')),

                       # Uncomment the admin/doc line below to enable admin documentation:
                       # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

                       # Uncomment the next line to enable the admin:
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^misiones/(\d*)$', 'mission.views.mission'),
                       url(r'^usuario/(\w*)$', 'mission.views.member'),
                       url(r'^comentarios/(\d*)$', 'mission.views.comment'),
                       url(r'^ranking/$', 'mission.views.ranking'),
                       url(r'^login/$', 'mission.views.login_auth'),
                       url(r'^logout$', 'mission.views.logout_view'),
                       url(r'^perfil$', 'mission.views.profile'),
                       url(r'^$', 'mission.views.home'),




)
if settings.DEBUG:
    urlpatterns += patterns('',
                            url(r'^static/(?P<path>.*)$',
                                'django.views.static.serve', {
                                    'document_root': settings.MEDIA_ROOT,
                                }),
                            (r'^media/(?P<path>.*)$',
                             'django.views.static.serve', {
                                'document_root': settings.MEDIA_ROOT})
    )
