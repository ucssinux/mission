# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User


class Member(models.Model):
    user = models.ForeignKey(User)
    pic = models.ImageField(upload_to='photo/', verbose_name='Foto')
    biography = models.TextField(verbose_name='Acerca de mi')
    score = models.IntegerField(verbose_name='Puntaje', blank=True, null=True)

    def __unicode__(self):
        return self.user.username


class Category(models.Model):
    name = models.CharField(max_length=20, verbose_name='Nombre')
    medal_name = models.CharField(max_length=20,
                                  verbose_name='Nombre de medalla')
    medal_image = models.ImageField(upload_to='medals',
                                    verbose_name='imagen de medalla')
    medal_description = models.TextField(verbose_name='Descripción')

    def __unicode__(self):
        return self.name


class Mission(models.Model):
    PRIORITY_CHOICES = zip(range(1, 11), range(1, 11))
    SCORE_CHOICES = zip(range(1, 21), range(1, 21))
    name = models.CharField(max_length=20, verbose_name='Nombre')
    description = models.TextField(verbose_name=u'Descripción')
    start_at = models.DateTimeField(verbose_name=u'Fecha Creación',
                                    auto_now_add=True, blank=True)
    deadline = models.DateTimeField(verbose_name='Fecha Limite')
    priority = models.IntegerField(verbose_name='Prioridad',
                                   choices=PRIORITY_CHOICES)
    base_score = models.IntegerField(verbose_name='Puntaje Base',
                                     choices=SCORE_CHOICES)
    user = models.ForeignKey(User, verbose_name='Asignado a')
    category = models.ForeignKey(Category)

    def __unicode__(self):
        return self.name


class FeedBack(models.Model):
    SCORE_CHOICES = zip(range(1, 4), range(1, 4))
    mission = models.ForeignKey(Mission)
    comments = models.TextField(verbose_name='Comentarios')
    score = models.IntegerField(verbose_name='Puntaje', choices=SCORE_CHOICES)
    user = models.ForeignKey(User)

    def __unicode__(self):
        return self.mission.name