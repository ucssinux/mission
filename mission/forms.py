# -*- coding: utf-8 -*-
from models import FeedBack, Member
from django.forms import ModelForm
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm


class FeedBackForm(ModelForm):
    class Meta:
        model = FeedBack
        exclude = ('user', 'mission')

class EditProfileForm(ModelForm):
    class Meta:
        model = Member