from django.contrib import admin
from mission.models import Mission, FeedBack, Member, Category


class AdminMission(admin.ModelAdmin):
    list_display = ['name', 'start_at', 'deadline', 'priority', 'user',
                    'base_score']

class AdminMember(admin.ModelAdmin):
    exclude = ['score']
admin.site.register(Mission, AdminMission)
admin.site.register(FeedBack)
admin.site.register(Member, AdminMember)
admin.site.register(Category)
