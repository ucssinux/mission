# -*- coding: utf-8 -*-
#internal
from models import Mission, FeedBack, Member
from forms import FeedBackForm, EditProfileForm
from django.contrib.auth.models import User
from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout


def home(request):
    return render_to_response('home.html',
                              context_instance=RequestContext(request))


def mission(request, id):
    if id:
        mission = Mission.objects.get(id=id)
        template = 'mission_id.html'
    else:
        mission = Mission.objects.all()
        template = 'mission_all.html'
    data = {'missions': mission}
    return render_to_response(template, data,
                              context_instance=RequestContext(request))


def member(request, username):
    user = User.objects.get(username=username)
    member = Member.objects.get(user=user)
    data = {'user': member}
    return render_to_response('user_profile.html', data,
                              context_instance=RequestContext(request))


def comment(request, mission):
    if request.method == 'POST':
        form = FeedBackForm(request.POST)
        if form.is_valid():
            form = form.save(commit=False)
            current_mission = Mission.objects.get(id=mission)
            form.mission = current_mission
            form.user = request.user
            form.save()
    else:
        form = FeedBackForm()
    comment = FeedBack.objects.filter(mission=mission)
    data = {'comments': comment, 'form': form}
    print form.errors
    return render_to_response('comment.html', data,
                              context_instance=RequestContext(request))


def ranking(request):
    ranking = Member.objects.filter().order_by('id')
    data = {'ranking': ranking}
    return render_to_response('ranking.html', data,
                              context_instance=RequestContext(request))


def login_auth(request):
    mini = request.GET.get('mini')
    if mini:
        template = 'login-mini.html'
    else:
        template = 'login.html'

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        print username
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/perfil')
            else:
                return HttpResponseRedirect('/login')

    return render_to_response(template,
                              context_instance=RequestContext(request))


def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/')


@login_required(login_url='/login/')
def profile(request):
    mission = Mission.objects.filter(user=request.user)
    form_edit_profile = EditProfileForm()
    data = {'missions': mission, 'form_edit_profile': form_edit_profile}
    return render_to_response('profile.html',data,
                              context_instance=RequestContext(request))